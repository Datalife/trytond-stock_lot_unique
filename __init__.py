# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import stock


def register():
    Pool.register(
        stock.Lot,
        stock.LotReplaceAsk,
        module='stock_lot_unique', type_='model')
    Pool.register(
        stock.LotReplace,
        module='stock_lot_unique', type_='wizard')
