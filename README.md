datalife_stock_lot_unique
=========================

The stock_lot_unique module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_lot_unique/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_lot_unique)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
